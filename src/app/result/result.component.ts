import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() person;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onResultClick() {
    this.router.navigate(['details', encodeURIComponent(this.person.url)], { state: this.person });
  }

}
