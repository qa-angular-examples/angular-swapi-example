import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SwapiService {

  constructor(private http: HttpClient) { }

  getAllPeople() {
    return this.http.get('https://swapi.co/api/people');
  }

  getPersonByUrl(url) {
    return this.http.get(url);
  }
}
