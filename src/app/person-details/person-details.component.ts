import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SwapiService } from '../swapi.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit {

  public person;

  constructor(private aR: ActivatedRoute, private swapi: SwapiService) { }

  ngOnInit() {
    const url = decodeURIComponent(this.aR.snapshot.params.apiUrl);
    this.swapi.getPersonByUrl(url).subscribe((person) => {
      this.person = person;
    });
  }

}
