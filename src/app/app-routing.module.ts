import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AllResultsComponent } from './all-results/all-results.component';
import { PersonDetailsComponent } from './person-details/person-details.component';

const routes = [
    {
        path: 'home',
        component: AllResultsComponent
    },
    {
        path: 'details/:apiUrl',
        component: PersonDetailsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
