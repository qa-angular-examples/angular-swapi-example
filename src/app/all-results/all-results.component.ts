import { Component, OnInit } from '@angular/core';
import { SwapiService } from '../swapi.service';

@Component({
  selector: 'app-all-results',
  templateUrl: './all-results.component.html',
  styleUrls: ['./all-results.component.css']
})
export class AllResultsComponent implements OnInit {

  public allPeopleData;

  constructor(private swapi: SwapiService) {}

  ngOnInit() {
      this.swapi.getAllPeople().subscribe((data) => {
        this.allPeopleData = data;
      });
  }

}
